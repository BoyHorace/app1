##################
#
# VPC definition
#
##################

# create the VPC
resource "aws_vpc" "Pomelo_VPC" {
  cidr_block           = var.vpcCIDRblock
  instance_tenancy     = var.instanceTenancy 
  enable_dns_support   = var.dnsSupport 
  enable_dns_hostnames = var.dnsHostNames
  tags = {
    Name        = var.vpc_name
    creator     = var.vpc_creator
    region      = var.vpc_region
    environment = var.vpc_env
    atomation   = var.vpc_atomation
    account     = var.account_id
  }
}

output "vpc_id" {
  value = aws_vpc.Pomelo_VPC.id
}


################
# Public subnet
################

resource "aws_subnet" "Pomelo_public" {
  vpc_id                  = aws_vpc.Pomelo_VPC.id
  cidr_block              = var.publicsubnetCIDRblock
  map_public_ip_on_launch = var.mapPublicIP 
  availability_zone       = var.availabilityZone
  
  tags = {
    Name              = var.public_subnet_name
    
  }
}

output "public_subnet_id" {
  value = aws_subnet.Pomelo_public.id
}

#################
# Private subnet
#################

resource "aws_subnet" "Pomelo_private" {
  vpc_id                  = aws_vpc.Pomelo_VPC.id
  cidr_block              = var.privatesubnetCIDRblock
  availability_zone       = var.availabilityZone
  
  tags = {
    Name              = var.private_subnet_name

  }
}

output "private_subnet_id" {
  value = aws_subnet.Pomelo_private.id
}

################
# Internet Gateway
################
 
resource "aws_internet_gateway" "Pomelo_IGW" {
 vpc_id = aws_vpc.Pomelo_VPC.id
 tags = {
        Name = "Pomelo IGW"
  }
}

#######################
# Route Table
######################

resource "aws_route_table" "Pomelo_route_table" {
 vpc_id = aws_vpc.Pomelo_VPC.id
 tags = {
        Name = "Pomelo VPC Route Table"
 }
}

####################
# Internet Access
####################
resource "aws_route" "Pomelo_internet_access" {
  route_table_id         = aws_route_table.Pomelo_route_table.id
  destination_cidr_block = var.destinationCIDRblock
  gateway_id             = aws_internet_gateway.Pomelo_IGW.id
}

##############################
# Associate the Route Table with the Subnet
###############################

resource "aws_route_table_association" "Pomelo_VPC_association" {
  subnet_id      = aws_subnet.Pomelo_public.id
  route_table_id = aws_route_table.Pomelo_route_table.id
}


##########################
#  Security Group
############################
resource "aws_security_group" "Pomelo_Security_Group" {
  vpc_id       = aws_vpc.Pomelo_VPC.id
  name         = "Pomelo Security Group"
  description  = "Pomelo VPC Security Group"
  
  # allow ingress of port 22
  ingress {
    cidr_blocks = var.ingressCIDRblock  
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  } 
  
  # allow egress of all ports
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
   Name = "Pomelo Security Group"
   Description = "Pomelo VPC Security Group"
  }
}


#########################
# SSH Key Setup
#########################


resource "tls_private_key" "Pomelo_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
 # key_name   = var.key_name
  key_name  = "test_demo"

  public_key = tls_private_key.Pomelo_key.public_key_openssh
}




##########################
# Virtual Compute instances that run a web service
##########################



resource "aws_instance" "Pomelo_web" {
  count                       = var.instance_count
  ami                         = var.ami
  instance_type               = var.instance_type
  user_data                   = var.user_data
  subnet_id                   = aws_subnet.Pomelo_public.id
  vpc_security_group_ids      = [aws_security_group.Pomelo_Security_Group.id]
  key_name                    = aws_key_pair.generated_key.key_name
  associate_public_ip_address = true
  tags = {
   Name = "Pomelo WEB"
   
 }
}

##########################
#Virtual Compute instances that run a database
##########################

resource "aws_db_instance" "pomelo-rds" {
  allocated_storage    = 100
  db_subnet_group_name = "db-subnetgrp"
  engine               = "postgres"
  engine_version       = "11.5"
  identifier           = "pomelo-rds"
  instance_class       = "db.m5.large"
  password             = "password"
  skip_final_snapshot  = true
  storage_encrypted    = true
  username             = "postgres"
  backup_retention_period = var.backup_retention_period
  vpc_security_group_ids = [aws_security_group.Pomelo_Security_Group.id]
  
 }
