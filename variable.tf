##################
#
# VPC variable definition
#
##################

variable "vpcCIDRblock" {
    description = "CIDR for the VPC"
    type        = string
    default     = "10.0.0.0/19"
}

variable "dnsSupport" {
   description = "Should be true to enable DNS support in the Default VPC"
   type        = bool
   default     = true
}

variable "dnsHostNames" {
   description = "Should be true to enable DNS hostnames in the Default VPC"
   type        = bool
   default     = true
}

variable "instanceTenancy" {
   description = "A tenancy option for instances launched into the VPC"
   type        = string
   default     = "default"
    
}

variable "vpc_name"{
  description = "name of vpc"
  type        = string
  default     = "Pomelo VPC"
}

variable "vpc_creator"{
 description = "creator user name of resource"
 type        = string
 default     = "PRO"
}
variable "vpc_region"{
 description = "resource of region"
 type        = string
 default     = "us-east-1"
}
variable "vpc_env"{
 description = "environment for resource"
 type        = string
 default     = "dev"
}
variable "vpc_atomation"{
 type        = string
 default     = "terraform"
}
variable "account_id"{
 description = "account id for resource"
 type        = string
 default     = "1001"
}


################
# Variable Public subnet
################

variable "publicsubnetCIDRblock" {
    description = "CIDR block for subnet"
    type        = string
    default     = "10.0.1.0/24"
}

variable "mapPublicIP" {
   description = "Should be false if you do not want to auto-assign public IP on launch"
   type        = bool
   default     = true
}

variable "availabilityZone" {
    description = "Availability zones names or ids in the region"
    type        = string
    default     = "us-east-1a"
}

variable "public_subnet_name" {
  description = "tagging for the subnet"
  type        = string
  default     = "Pomelo_public_subnet"
}

################
# Variable private subnet
################

variable "privatesubnetCIDRblock" {
    description = "CIDR block for subnet"
    type        = string
    default     = "10.0.2.0/24"
}

variable "private_subnet_name" {
  description = "tagging for the subnet"
  type        = string
  default     = "Pomelo_private_subnet"
}


####################
# Variable Internet Access
####################

variable "destinationCIDRblock" {
    
    default = "0.0.0.0/0"
}

##########################
#  Variable Security Group
############################

variable "ingressCIDRblock" {
    type = list
    default = [ "0.0.0.0/0" ]
}

#########################
# Variable SSH Key Setup
#########################

#variable "key_name" {
#  key_name = "TESTMODE"
#}


##########################
# variable Virtual Compute instances
##########################

variable "instance_count" {
  description = "Number of instances to launch"
  type        = number
  default     = 1
}

variable "ami" {
  description = "ID of AMI to use for the instance"
  type        = string
  default     = "ami-0915e09cc7ceee3ab"
}
  
variable "instance_type" {
  description = "The type of instance to start"
  type        = string
  default     = "t2.micro"
}

variable "user_data" {
  description = "The user data to provide when launching the instance."
  type        = string
  default     = null
}


##########################
# Variable of DB 
##########################

variable "backup_retention_period" {
  description = "The days to retain backups for"
  type        = number
  default     = 1
}

